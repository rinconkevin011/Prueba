/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socketobjetos;

import java.io.Serializable;

/**
 *
 * @author informatica
 */
public class Gasolinera extends Thread implements Serializable{
    public double disel;
    public double gasolinaPlomo;
    public double gasolinaOptima;

    public Gasolinera(double disel,  double gasolinaPlomo, double gasolinaOptima) {
        this.disel = disel;
        this.gasolinaPlomo = gasolinaPlomo;
        this.gasolinaOptima = gasolinaOptima;
    }

    public double getDisel() {
        return disel;
    }

    public void setDisel(double disel) {
        this.disel = disel;
    }

    public double getGasolinaPlomo() {
        return gasolinaPlomo;
    }

    public void setGasolinaPlomo(double gasolinaPlomo) {
        this.gasolinaPlomo = gasolinaPlomo;
    }

    public double getGasolinaOptima() {
        return gasolinaOptima;
    }

    public void setGasolinaOptima(double gasolinaOptima) {
        this.gasolinaOptima = gasolinaOptima;
    }
    
    
    
}
