/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socketobjetos;

import java.io.DataInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Observable;

/**
 *
 * @author informatica
 */
public class Servidor extends Observable implements Runnable{
    private ArrayList<Socket>  Clientes;
    private int PUERTO;
    
    public Servidor(int puerto){
        this.PUERTO = puerto;
        this.Clientes = new ArrayList();
    }
    @Override
    public void run(){
        ServerSocket servidor = null;
        Socket sc = null;
        
        try {
            servidor = new ServerSocket(PUERTO);
            System.out.println("servidor iniciado");
            
            while (true) {                
                sc = servidor.accept();
                System.err.println("cliente conectado");
                
                Clientes.add(sc);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public void envioInfo(Gasolinera g){
        System.out.println(g.getDisel()+"");
        for (Socket sock : Clientes ){
            try {
               ObjectOutputStream oss = new ObjectOutputStream(sock.getOutputStream());
               oss.writeObject(g);
               
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }
}
